﻿$(function () {
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'trandau',
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 0,
            beta: 30,
            depth: 50,
            viewDistance: 25
        },
    },
    title: {
		text: 'Thống kê<br>2017',
		align: 'center',
		verticalAlign: 'middle',
		y: 40,
		fontSize: '8px'
	},
    yAxis: {
        max: 200,
        title: {
            text: null
        }
    },
    tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
    

    plotOptions: {
    pie: {
      dataLabels: {
        enabled: true,
        distance: 0,
        style: {
          fontWeight: 'bold',
          color: 'white'
        }
      },
      startAngle: -90,
      endAngle: 90,
      center: ['50%', '75%'],
      size: '110%'
    }
  },

	series: [{
    type: 'pie',
    name: 'Thống kê 2017',
    innerSize: '50%',
    data: [
      ['Quý I', 1168],
      ['Quý II', 290],
      ['Quý III', 301],
      ['Quý IV', 175]
		]	
	}],
    credits: {
        enabled: false
    },
    exporting: { enabled: false }
});

function showValues() {
    $('#alpha-value').html(chart.options.chart.options3d.alpha);
    $('#beta-value').html(chart.options.chart.options3d.beta);
    $('#depth-value').html(chart.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
    chart.options.chart.options3d[this.id] = parseFloat(this.value);
    showValues();
    chart.redraw(false);
});

showValues();
});