﻿$(function () {
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'chay'
    },
    title: {
        text: null,
        style: {
            color: '#000000',
            fontWeight: 'bold'
        }
    },
    subtitle: {
        text: null
    },
    xAxis:{
        categories: ['2014', '2015', '2016', '2017']
    },
    yAxis: {
        title: {
            text: null
        }
    },
	legend: {
        align: 'right',
        verticalAlign: 'top',
        layout: 'vertical',
    },
    plotOptions: {
		area: {
		  pointStart: 1940,
		  marker: {
			enabled: false,
			symbol: 'circle',
			radius: 2,
			states: {
			  hover: {
				enabled: true
			  }
			}
		  }
		}
	},
    series: [{
        name: 'TP Bắc Ninh',
        data: [502, 635, 809, 947]
    }, {
        name: 'Quế Võ',
        data: [106, 107, 111, 133]
    }, {
        name: 'Tiên Du',
        data: [163, 203, 276, 408,]
    }, {
        name: 'Thuận Thành',
        data: [18, 31, 54, 156, ]
    }],
    credits: {
        enabled: false
    },
    exporting: { enabled: false }
});

function showValues() {
    $('#alpha-value').html(chart.options.chart.options3d.alpha);
    $('#beta-value').html(chart.options.chart.options3d.beta);
    $('#depth-value').html(chart.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
    chart.options.chart.options3d[this.id] = parseFloat(this.value);
    showValues();
    chart.redraw(false);
});

showValues();
});