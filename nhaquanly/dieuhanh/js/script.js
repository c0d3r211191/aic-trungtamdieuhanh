﻿var lat = 21.149589;
var lon = 106.0786883; 
var map = new L.Map('map', {

        zoom: 15,
        minZoom: 6,
});



    // create a new tile layer
    var tileUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 18
    });

    // add the layer to the map
    map.addLayer(layer);

    var cuuthuong = getParameterByName('ct');
    var cuuhoa = getParameterByName('ch');
    var chihuy = getParameterByName('chh');


    var xethang1 = [[lat + 0.02, lon - 0.03], [lat + 0.07, lon + 0.07], [lat + 0.07, lon + 0.07], [lat + 0.02, lon - 0.03]];
    var xethang2 = [[lat - 0.040, lon + 0.033], [lat - 0.015, lon - 0.015], [lat + 0.02, lon - 0.02], [lat - 0.015, lon - 0.015],[lat - 0.040, lon + 0.033]];
    var xebot1 = [[lat + 0.01, lon - 0.02], [lat + 0.041, lon - 0.047], [lat + 0.041, lon - 0.047], [lat + 0.01, lon - 0.02]];
    var xebot2 = [[lat - 0.032, lon + 0.068], [lat + 0.02, lon - 0.02], [lat + 0.02, lon - 0.02], [lat - 0.032, lon + 0.068]];
    var xethang5 = [[lat + 0.09, lon - 0.04], [lat + 0.0962, lon - 0.0016], [lat + 0.068, lon - 0.0064], [lat + 0.02, lon - 0.02],  [lat + 0.02, lon - 0.02] ,  [lat + 0.068, lon - 0.0064],  [lat + 0.0962, lon - 0.0016], [lat + 0.09, lon - 0.04] ];
    var xethang6 = [[lat + 0.002, lon - 0.07], [lat - 0.011, lon - 0.055], [lat + 0.02, lon - 0.02], [lat + 0.02, lon - 0.02], [lat - 0.0118, lon - 0.055], [lat + 0.002, lon - 0.07]];

    var xecuuthuong1 = [[lat - 0.02, lon + 0.02], [lat + 0.06, lon + 0.08], [lat + 0.06, lon + 0.08], [lat - 0.02, lon + 0.02]];
    var xecuuthuong2 = [[lat - 0.01, lon + 0.02], [lat + 0.03, lon + 0.075], [lat + 0.03, lon + 0.075], [lat - 0.01, lon + 0.02]];
    var xecuuthuong3 = [[lat - 0.07218, lon - 0.076218], [lat - 0.065009, lon - 0.053655], [lat - 0.031934, lon - 0.047476], [lat - 0.004908, lon - 0.027906], [lat - 0.002727, lon - 0.004292]];
    var xecuuthuong4 = [[lat + 0.038468, lon - 0.156898], [lat + 0.042758, lon - 0.11879], [lat + 0.055709, lon - 0.068836], [lat + 0.032826, lon - 0.030384], [lat - 0.003216, lon - 0.00368]];

    //var mymap = [[lat, lon], [lat + 0.12, lon + 0.12],[lat - 0.12, lon - 0.12]];

    var londonBrusselFrankfurtAmsterdamLondon = [[51.507222, -0.1275], [50.85, 4.35],
    [50.116667, 8.683333], [52.366667, 4.9], [51.507222, -0.1275]];

    var barcelonePerpignanPauBordeauxMarseilleMonaco = [
        [41.385064, 2.173403],
        [42.698611, 2.895556],
        [43.3017, -0.3686],
        [44.837912, -0.579541],
        [43.296346, 5.369889],
        [43.738418, 7.424616]
    ];


//map.fitBounds(mymap);

    map.setView([lat, lon], 11);

    var FireIcon = L.icon({
        iconUrl: 'images/fire-2-32_2.gif',
        iconSize: [36, 36], // size of the icon
    });

    var CCIcon = L.icon({
        iconUrl: 'images/Ol_icon_blue_example.png',
        iconSize: [32, 32], // size of the icon
    });

    var CTIcon = L.icon({
        iconUrl: 'images/Ol_icon_red_example.png',
        iconSize: [32, 32], // size of the icon
    });

    var CHIcon = L.icon({
        iconUrl: 'images/jeep.png',
        iconSize: [32, 32], // size of the icon
    });

    var marker = L.marker([lat, lon], { icon: FireIcon }).addTo(map);

    if (cuuhoa == 1) {
        //========================================================================
        var marker1 = L.Marker.movingMarker(xethang1,
            [40000, 12000, 40000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker1.loops = 0;
        marker1.bindPopup('Xe thang 1 <br/> <video width="160" height="90" controls=controls autoplay=autoplay muted=muted><source src="videos/113.mp4" type="video/mp4" /></video>', { closeOnClick: false });
        marker1.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            //marker1.getPopup().setContent("Xe thang 1")
            marker1.openPopup();
            setTimeout(function () {
                marker1.closePopup();
            }, 200);
            //}
        });
        marker1.once('click', function () {
            marker1.start();
            marker1.closePopup();
            marker1.unbindPopup();
            marker1.on('click', function () {
                if (marker1.isRunning()) {
                    marker1.pause();
                } else {
                    marker1.start();
                }
            });
        });

        //========================================================================

        var marker2 = L.Marker.movingMarker(xethang2,
            [50000, 40000, 40000, 50000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker2.loops = 0;
        marker2.bindPopup('Xe thang 2', { closeOnClick: false });
        marker2.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            marker2.getPopup().setContent("Xe thang 2")
            marker2.openPopup();
            setTimeout(function () {
                marker2.closePopup();
            }, 200);
            //}
        });
        marker2.once('click', function () {
            marker2.start();
            marker2.closePopup();
            marker2.unbindPopup();
            marker2.on('click', function () {
                if (marker2.isRunning()) {
                    marker2.pause();
                } else {
                    marker2.start();
                }
            });
        });

        //=========================================================================

        var marker3 = L.Marker.movingMarker(xebot1,
            [16000, 11000, 16000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker3.loops = 0;
        marker3.bindPopup('Xe bọt 1', { closeOnClick: false });
        marker3.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            marker3.getPopup().setContent("Xe bọt 1")
            marker3.openPopup();
            setTimeout(function () {
                marker3.closePopup();
            }, 200);
            //}
        });
        marker3.once('click', function () {
            marker3.start();
            marker3.closePopup();
            marker3.unbindPopup();
            marker3.on('click', function () {
                if (marker3.isRunning()) {
                    marker3.pause();
                } else {
                    marker3.start();
                }
            });
        });

        var marker4 = L.Marker.movingMarker(xebot2,
            [16000, 16000, 16000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker4.loops = 0;
        marker4.bindPopup('Xe bọt 2', { closeOnClick: false });
        marker4.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            marker4.getPopup().setContent("Xe bọt 2")
            marker4.openPopup();
            setTimeout(function () {
                marker4.closePopup();
            }, 200);
            //}
        });
        marker4.once('click', function () {
            marker4.start();
            marker4.closePopup();
            marker4.unbindPopup();
            marker4.on('click', function () {
                if (marker4.isRunning()) {
                    marker4.pause();
                } else {
                    marker4.start();
                }
            });
        });

        var marker5 = L.Marker.movingMarker(xethang5,
            [16000, 16000, 16000, 16000, 16000, 16000, 16000, 16000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker5.loops = 0;
        marker5.bindPopup('Xe thang 5', { closeOnClick: false });
        marker5.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            marker5.getPopup().setContent("Xe thang 5")
            marker5.openPopup();
            setTimeout(function () {
                marker5.closePopup();
            }, 200);
            //}
        });
        marker5.once('click', function () {
            marker5.start();
            marker5.closePopup();
            marker5.unbindPopup();
            marker5.on('click', function () {
                if (marker5.isRunning()) {
                    marker5.pause();
                } else {
                    marker5.start();
                }
            });
        });

        var marker6 = L.Marker.movingMarker(xethang6,
            [16000, 16000, 16000,16000,16000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker6.loops = 0;
        marker6.bindPopup('Xe bọt 2', { closeOnClick: false });
        marker6.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            marker6.getPopup().setContent("Xe bọt 2")
            marker6.openPopup();
            setTimeout(function () {
                marker6.closePopup();
            }, 200);
            //}
        });
        marker6.once('click', function () {
            marker6.start();
            marker6.closePopup();
            marker6.unbindPopup();
            marker6.on('click', function () {
                if (marker6.isRunning()) {
                    marker6.pause();
                } else {
                    marker6.start();
                }
            });
        });
    }
    if (cuuthuong == 1) {
        var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
            [32000, 10000, 32000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

        markerCT1.loops = 0;
        markerCT1.bindPopup('Xe cứu thương 1', { closeOnClick: false });
        markerCT1.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            markerCT1.getPopup().setContent("Xe cứu thương 1")
            markerCT1.openPopup();
            setTimeout(function () {
                markerCT1.closePopup();
            }, 200);
            //}
        });
        markerCT1.once('click', function () {
            markerCT1.start();
            markerCT1.closePopup();
            markerCT1.unbindPopup();
            markerCT1.on('click', function () {
                if (markerCT1.isRunning()) {
                    markerCT1.pause();
                } else {
                    markerCT1.start();
                }
            });
        });

        var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
            [35000, 30000, 35000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

        markerCT2.loops = 0;
        markerCT2.bindPopup('Xe cứu thương 2', { closeOnClick: false });
        markerCT2.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            markerCT2.getPopup().setContent("Xe cứu thương 2")
            markerCT2.openPopup();
            setTimeout(function () {
                markerCT2.closePopup();
            }, 200);
            //}
        });

        markerCT2.once('click', function () {
            markerCT2.start();
            markerCT2.closePopup();
            markerCT2.unbindPopup();
            markerCT2.on('click', function () {
                if (markerCT2.isRunning()) {
                    markerCT2.pause();
                } else {
                    markerCT2.start();
                }
            });
        });

        var markerCT3 = L.Marker.movingMarker(xecuuthuong3,
            [35000, 35000, 35000, 35000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

        markerCT3.loops = 0;
        markerCT3.bindPopup('Xe cứu thương 3', { closeOnClick: false });
        markerCT3.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            markerCT3.getPopup().setContent("Xe cứu thương 3")
            markerCT3.openPopup();
            setTimeout(function () {
                markerCT3.closePopup();
            }, 200);
            //}
        });

        markerCT3.once('click', function () {
            markerCT3.start();
            markerCT3.closePopup();
            markerCT3.unbindPopup();
            markerCT3.on('click', function () {
                if (markerCT3.isRunning()) {
                    markerCT3.pause();
                } else {
                    markerCT3.start();
                }
            });
        });
        var markerCT4 = L.Marker.movingMarker(xecuuthuong4,
            [35000, 35000, 35000, 35000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

        markerCT4.loops = 0;
        markerCT4.bindPopup('Xe cứu thương 4', { closeOnClick: false });
        markerCT4.on('loop', function (e) {
            //if (e.elapsedTime < 50) {
            markerCT4.getPopup().setContent("Xe cứu thương 4")
            markerCT4.openPopup();
            setTimeout(function () {
                markerCT4.closePopup();
            }, 200);
            //}
        });

        markerCT4.once('click', function () {
            markerCT4.start();
            markerCT4.closePopup();
            markerCT4.unbindPopup();
            markerCT4.on('click', function () {
                if (markerCT4.isRunning()) {
                    markerCT4.pause();
                } else {
                    markerCT4.start();
                }
            });
        });
    }

    if (chihuy == 1)
    {
        var markerCH = L.marker([lat - 0.02, lon - 0.02], { icon: CHIcon }).addTo(map);
    }
 
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
